### 
##Se adjunta imagen para la solución del ejercio 1 - Modelo base de datos de universidad.
## Archivo llamado ModeloEjercicio1.pn
###########################################################################################

##Reglas para la ejecución del Backend

#1. en archivo applications yml está definida la información para insertar en BD MySQL
#2. Se debe habilitar el comando initialization-mode: always, para realizar la insersión de datos
#   en tablas maestras (Tipo de personas, monedas y Tipos de movimientos)
#   Para las posteriores ejecuciones se debe deshabilitar.

# EL aplicativo Back Correo por el puerto 7777 




##############################################################################
#A continuación los CURL de respuesta de los ejercicios


################################################################################################################################
#Ejercicio 1 - Gestión de Titulares

## 1. Crear Titular con persona jurídica
curl -X POST "http://localhost:7777/api/accountowner" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg" -d "{\"creationTimestamp\":\"2020-07-03T18:54:58.243Z\",\"typePerson\":{\"id\":2},\"rut\":123456789,\"legalPerson\":{\"businessName\":\"Cerevro app\",\"foundationDate\":\"2020-07-03\"}}"

## 2. Crear Titular con persona natural
curl -X POST "http://localhost:7777/api/accountowner" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg" -d "{\"creationTimestamp\":\"2020-07-03T18:57:30.243Z\",\"typePerson\":{\"id\":1},\"rut\":\"AB123456789\",\"naturalPerson\":{\"name\":\"Julieta\",\"lastName\":\"Espinosa Marin\",\"dni\":\"987456321\"}}"

## 3. Editar Titular con persona natural
curl -X PUT "http://localhost:7777/api/accountowner" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg" -d "{\"modificationTimestamp\":\"2020-07-03T18:59:20.243Z\",\"typePerson\":{\"id\":1},\"rut\":\"AB123456789\",\"naturalPerson\":{\"name\":\"Julieta Maria\",\"lastName\":\"Espinosa Marin\",\"dni\":\"987456321\"},\"id\":9}"

## 4. Editar Titular con persona jurídica
curl -X PUT "http://localhost:7777/api/accountowner" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg" -d "{\"creationTimestamp\":\"2020-07-03T18:54:58.243Z\",\"typePerson\":{\"id\":2},\"rut\":123456789,\"legalPerson\":{\"businessName\":\"Cerevro.app\",\"foundationDate\":\"2020-07-03\"},\"id\":8}"

## 5. Obtener todos titulares
curl -X GET "http://localhost:7777/api/accountowner" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 6. Obtener titulares por id
curl -X GET "http://localhost:7777/api/accountowner/9" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 7. Eliminar titular por id
curl -X DELETE "http://localhost:7777/api/accountowner/9" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 8. Obtener maestro de tipo de personas
curl -X GET "http://localhost:7777/api/type/person" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

##############################################################################################################################

#Ejercicio 2 - Gestión de Cuentas y movimientos

## 1. Creación de una nueva cuenta
curl -X POST "http://localhost:7777/api/account" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"  -d "{\"creationTimestamp\":\"2020-07-03T19:10:30.654Z\",\"currency\":{\"id\":1},\"balance\":\"689521547.32\",\"accountNumber\":654123987}"

## 2. Eliminar una cuenta por id
curl -X DELETE "http://localhost:7777/api/account/18" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 3. Obtener todas las cuentas registradas y sus movimientos respectivos
curl -X GET "http://localhost:7777/api/account" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 4. Agregar Movimiento a cuenta
curl -X POST "http://localhost:7777/api/account/movement" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"  -d "{\"movementType\":{\"id\":2},\"account\":{\"id\":17},\"description\":\"TV sin IVA\",\"creationTimestamp\":\"2020-07-03T19:23:15.684Z\",\"amount\":\"321.45\"}"

## 5. Listar movimientos de cuenta ordenados descendentes por fecha
curl -X GET "http://localhost:7777/api/account/movement/byaccount/17" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 6. Obtener cuenta por Id
curl -X GET "http://localhost:7777/api/account/17" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 7. Obtener maestro de monedas
curl -X GET "http://localhost:7777/api/type/currency" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"

## 8. Obtener maestro de tipos de movimiento
curl -X GET "http://localhost:7777/api/type/movement" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiZXhwIjoxNTkzODI0MjkyLCJpYXQiOjE1OTM4MDYyOTJ9.pKtlWtoOiWYdCHGFCRlZu3K3BK_q70yWUtih5EOJcr5r3sUEQausLDhRHzBTK1XTReauxlBehoWezdPIVAV5Vg"









