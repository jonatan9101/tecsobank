INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('AR','ARGENTINA', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('BR','BRASIL', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('UY','URUGUAY', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('CH','CHILE', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO person_type (description_type, creation_timestamp, modification_timestamp, version_number) 
VALUES ('Persona Natural', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO person_type (description_type, creation_timestamp, modification_timestamp, version_number) 
VALUES ('Persona Juridica', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movement_type (description_type, creation_timestamp, modification_timestamp, version_number) 
VALUES ('Débito', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movement_type (description_type, creation_timestamp, modification_timestamp, version_number) 
VALUES ('Crédito', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO currency (currency, short_name, min_balance, creation_timestamp, modification_timestamp, version_number) 
VALUES ('Dólar', 'US', 300, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO currency (currency, short_name, min_balance, creation_timestamp, modification_timestamp, version_number) 
VALUES ('Euro', 'EUR', 150, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO currency (currency, short_name, min_balance, creation_timestamp, modification_timestamp, version_number) 
VALUES ('Peso', 'COP', 1000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);
