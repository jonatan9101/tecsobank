
#### CONSIDERACIONES GENERALES ####

- En términos generales, el sistema que se trabaja en este proyecto es del ámbito bancario, involucrando la gestión de titulares de cuentas, cuentas y movimientos asociados a las mismas.

- Todos los ejercicios se resuelven en el mismo proyecto, no es necesario separarlos.

- Se debe desarrollar un frontend para todas las funcionalidades (Angular, React, etc... algún framework basado en javascript). No es necesario trabajar sobre consideraciones estéticas o de interfaz de usuario
avanzadas. Será suficiente con que se implemente una interfaz básica, ordenada y completa que permita probar las funcionalidades.


=====================================================================================================================================================================================
#### Ejercicio 1 ####

Desarollar todos los artefactos necesarios en el backend (controller, servicio, repositorio, modelo, etc) y proyecto de frontend para implementar la "gestión de titulares de cuentas corrientes", según las siguientes consideraciones:

- Un titular puede ser tanto una persona física como jurídica.
	
- Si el titular es una persona física, los atributos requeridos serán los siguientes: nombre (máximo 80 caracteres), apellido (máximo 250 caracteres) y CC.
	
- Si el titular es una persona jurídica, los atributos requeridos serán los siguientes: razón social (máximo 100 caracteres) y año de fundación.
	
- Tanto para las personas físicas como jurídicas, se requiere RUT.
	
- No pueden existir 2 titulares con el mismo RUT.



=====================================================================================================================================================================================
#### Ejercicio 2 ####

Desarollar todos los artefactos necesarios en el backend (controller, servicio, repositorio, modelo, etc) y proyecto de frontend para implementar la "gestión de cuentas y movimientos", según las siguientes consideraciones:

- Una cuenta corriente puede tener n movimientos.

- Los movimientos no pueden eliminarse ni modificarse.

- Las cuentas solo pueden eliminarse si no tienen movimientos asociados.

- Las cuentas poseen un número de cuenta (valor requerido y único), una moneda (peso, dolar, euro) y un saldo (valor numérico de 2 decimales).

- Los movimientos poseen fecha y hora, tipo de movimiento (débito o crédito), descripción (200 caracteres) e importe (numérico de 2 decimales).

- Cada vez que se incorpora un nuevo movimiento se debe actualizar el saldo de la cuenta asociada.

- Si la aplicación de un movimiento fuera a generar un saldo negativo en la cuenta mayor a 1000 (para cuentas en pesos), 300 (para cuentas en dólares) o 150 (para cuentas en euros)... se deberá rechazar.


En la capa REST, los endpoints requeridos son los siguientes:
   1. *crear cuenta*
   2. *eliminar cuenta*
   3. *listar cuentas*
   4. *agregar movimiento*
   5. *listar movimientos x cuenta* (ordenados de forma decreciente, por fecha)

Se deberán aplicar todas las validaciones funcionales que se consideren oportunas, informando los errores de manera conveniente.



=====================================================================================================================================================================================
#### Ejercicio 3 ####
#### Funcionalidades adicionales ####

Implementar la mayor cantidad de funcionalidades adicionales posibles.

Ejemplos/propuestas:

- Registración y autenticación de usuario.

- Mecanismo de autorización de acceso a las funcionalidades desarrolladas.

- Pruebas unitarias (Junit) de los distintos componentes.

- Reemplazar la base de datos configurada en memoria (H2) por una a elección de entre las siguientes: PostgreSQL o MySQL.
	De hacerlo, la solución deberá incluir:
		- Información sobre la BD utilizada.
		- Script de creación e inicialización de datos.
		- Modificación correspondiente en la configuración del datasource.
		- Cualquier otra información que considere necesaria.

- etc (cualquier funcionalidad adicional que el candidato considere pertinente).


