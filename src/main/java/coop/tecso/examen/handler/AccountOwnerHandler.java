package coop.tecso.examen.handler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import coop.tecso.examen.shared.exceptions.InvalidRequestFieldException;
import coop.tecso.examen.shared.utils.ErrorInfo;
import coop.tecso.examen.shared.utils.Time;

@ControllerAdvice
public class AccountOwnerHandler {


	
   @ExceptionHandler(InvalidRequestFieldException.class)
   public ResponseEntity<ErrorInfo> invalidRequestFieldException(HttpServletRequest request, InvalidRequestFieldException e) {
       ErrorInfo errorInfo = new ErrorInfo(Time.getTime(),e.getStatus().value(), e.getMessage(), request.getRequestURI());
       return new ResponseEntity<>(errorInfo, e.getStatus());
   }
   
   @ExceptionHandler(TransactionSystemException.class)
   public ResponseEntity<ErrorInfo> constraintViolationException(HttpServletRequest request, TransactionSystemException e) {
	   Throwable cause = ((TransactionSystemException) e).getRootCause();
	   final List<String> errors = new ArrayList<String>();
	    if (cause instanceof ConstraintViolationException) {        

	        ConstraintViolationException consEx= (ConstraintViolationException) cause;
	        
	        for (final ConstraintViolation<?> violation : consEx.getConstraintViolations()) {
	            errors.add(violation.getPropertyPath() + ": " + violation.getMessage());
	            
	        }
	    }
	    ErrorInfo errorInfo = new ErrorInfo(Time.getTime(),HttpStatus.BAD_REQUEST.value(), e.getLocalizedMessage(), request.getRequestURI());
        return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
   }
}