package coop.tecso.examen.repository;


import org.springframework.data.repository.CrudRepository;

import coop.tecso.examen.model.Currency;



public interface CurrencyRepository extends CrudRepository<Currency, Long> {

}
