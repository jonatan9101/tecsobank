package coop.tecso.examen.repository;


import org.springframework.data.repository.CrudRepository;

import coop.tecso.examen.model.AccountOwner;



public interface AccountOwnerRepository extends CrudRepository<AccountOwner, Long> {

	AccountOwner findByRut(String rut);
	
}
