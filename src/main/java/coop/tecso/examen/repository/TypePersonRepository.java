package coop.tecso.examen.repository;


import org.springframework.data.repository.CrudRepository;

import coop.tecso.examen.model.PersonType;

public interface TypePersonRepository extends CrudRepository<PersonType, Long> {

}
