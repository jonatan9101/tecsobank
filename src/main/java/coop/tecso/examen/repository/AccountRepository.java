package coop.tecso.examen.repository;


import org.springframework.data.repository.CrudRepository;

import coop.tecso.examen.model.Account;



public interface AccountRepository extends CrudRepository<Account, Long> {

	Account findByAccountNumber(long accountNumber);
	
}
