package coop.tecso.examen.shared.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidRequestFieldException extends RuntimeException {

	private static final long serialVersionUID = 2523731481754195970L;
	
	private String message;
	
	private HttpStatus status;
	
	public InvalidRequestFieldException() {
		
	}
	
	public InvalidRequestFieldException(String message) {
		super(message);
	}
	
	public InvalidRequestFieldException(String message, HttpStatus status) {
		this.message=message;
		this.status=status;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
