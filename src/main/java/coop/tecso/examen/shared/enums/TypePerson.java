package coop.tecso.examen.shared.enums;

public enum TypePerson {

	NATURAL(1),
	LEGAL(2)
	;
	
	private int id;
	
	private TypePerson(int id) {
		this.id=id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
