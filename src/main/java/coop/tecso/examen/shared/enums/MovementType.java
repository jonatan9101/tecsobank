package coop.tecso.examen.shared.enums;

public enum MovementType {

	DEBITO(1),
	CREDITO(2)
	;
	
	private int id;
	
	private MovementType(int id) {
		this.id=id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
