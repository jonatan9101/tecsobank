package coop.tecso.examen.shared.utils;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorInfo {

		@JsonProperty("timestamp")
		private Timestamp timestamp;
		
	   @JsonProperty("message")
	   private String message;
	   @JsonProperty("status_code")
	   private int statusCode;
	   @JsonProperty("uri")
	   private String uriRequested;


	   public ErrorInfo(Timestamp timestamp,int statusCode, String message, String uriRequested) {
	       this.timestamp=timestamp;
		   this.message = message;
	       this.statusCode = statusCode;
	       this.uriRequested = uriRequested;
	   }

	   
	   
	   public Timestamp getTimestamp() {
		   return timestamp;
	   }



	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}



	public String getMessage() {
	       return message;
	   }

	   public int getStatusCode() {
	       return statusCode;
	   }

	   public String getUriRequested() {
	       return uriRequested;
	   }

	}