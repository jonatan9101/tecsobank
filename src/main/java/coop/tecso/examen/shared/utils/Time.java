package coop.tecso.examen.shared.utils;

import java.sql.Timestamp;

public class Time {

	public static Timestamp getTime() {
		return new Timestamp(System.currentTimeMillis());
	} 
	
}
