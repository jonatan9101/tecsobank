package coop.tecso.examen.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.model.Account;
import coop.tecso.examen.model.Movement;
import coop.tecso.examen.service.impl.AccountServiceImpl;
import coop.tecso.examen.service.impl.MovementServiceImpl;
import coop.tecso.examen.shared.enums.MovementType;
import coop.tecso.examen.shared.exceptions.InvalidRequestFieldException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountServiceImpl accountServiceImpl;
	
	@Autowired
	private MovementServiceImpl movementServiceImpl;
	
	
	@PostMapping(value="")
	public ResponseEntity<Long> saveAccount(@Valid @RequestBody Account account) {
		if(accountServiceImpl.findByAccountNumber(account.getAccountNumber())!=null) 
			throw new InvalidRequestFieldException("El Número de cuenta enviado ya se encuentra registrado",HttpStatus.CONFLICT);
		accountServiceImpl.save(account);
		return new ResponseEntity<Long>(account.getId(),HttpStatus.CREATED);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Long> deleteAccount(@PathVariable("id") long id) {
		Account currentaccount=accountServiceImpl.findById(id);
		if(currentaccount==null)
			throw new InvalidRequestFieldException("El id "+id+" no se encuentra registrado como cuenta",HttpStatus.NOT_FOUND);
		if(currentaccount.getMovements().size()>0)
			throw new InvalidRequestFieldException("El id "+id+" ya tiene movimientos registrados",HttpStatus.CONFLICT);
		accountServiceImpl.deleteById(id);
		return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(value = "",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Account>> getAllAccount() {
		List<Account> accountsList=accountServiceImpl.findAllAccounts();
		return new ResponseEntity<List<Account>>(accountsList,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> getByIdAccount(@PathVariable("id") long id) {
		Account account=accountServiceImpl.findById(id);
		return new ResponseEntity<Account>(account,HttpStatus.OK);
	}
	
	@PostMapping(value="/movement")
	public ResponseEntity<Long> saveMovement(@Valid @RequestBody Movement movement) {
		Account currentAccount=accountServiceImpl.findById(movement.getAccount().getId());
		double newBalance=0;
		if(movement.getMovementType().getId()==MovementType.DEBITO.getId()) {
			newBalance=currentAccount.getBalance()+movement.getAmount();
		}else if(movement.getMovementType().getId()==MovementType.CREDITO.getId()) {
			newBalance=currentAccount.getBalance()-movement.getAmount();
		}else {
			throw new InvalidRequestFieldException("El tipo de movimiento enviado no se encuentra en el Maestro",HttpStatus.BAD_REQUEST);
		}
		
		if(newBalance<currentAccount.getCurrency().getMinBalance())
			throw new InvalidRequestFieldException("La operación genera un saldo mínimo menor al permitido para las cuentas en "+currentAccount.getCurrency().getCurrency(),HttpStatus.BAD_REQUEST);
		movementServiceImpl.save(movement);
		currentAccount.setBalance(newBalance);
		accountServiceImpl.save(currentAccount);
		return new ResponseEntity<Long>(movement.getId(),HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/movement/byAccount/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Movement>> getMovementsByIdAccount(@PathVariable("id") long id) {
		Account account=accountServiceImpl.findById(id);
		if(account==null)
			throw new InvalidRequestFieldException("El id "+id+" no se encuentra registrado como cuenta",HttpStatus.NOT_FOUND);
		List<Movement> movementsList=movementServiceImpl.findByAccountOrderByMovementDateDesc(account);
		return new ResponseEntity<List<Movement>>(movementsList,HttpStatus.OK);
	}
	
	
	
}
