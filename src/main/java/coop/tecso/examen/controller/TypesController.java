package coop.tecso.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.model.Currency;
import coop.tecso.examen.model.MovementType;
import coop.tecso.examen.model.PersonType;
import coop.tecso.examen.service.impl.CurrencyServiceImpl;
import coop.tecso.examen.service.impl.MovementTypeServiceImpl;
import coop.tecso.examen.service.impl.TypePersonServiceImpl;

@CrossOrigin("*")
@RestController
@RequestMapping("/type")
public class TypesController {

	@Autowired
	private TypePersonServiceImpl typePersonServiceImpl;

	@Autowired
	private CurrencyServiceImpl currencyServiceImpl;
	
	@Autowired
	private MovementTypeServiceImpl movementTypeServiceImpl;
	
	// Get All types Person
	@RequestMapping(value = "/person", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PersonType>> findAllTyPersons() {
		List<PersonType> typePersonsList=typePersonServiceImpl.findAllTypePersons();
		return new ResponseEntity<List<PersonType>>(typePersonsList,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/currency", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Currency>> findAllCurrencies() {
		List<Currency> currenciesList=currencyServiceImpl.findAllCurrencies();
		return new ResponseEntity<List<Currency>>(currenciesList,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/movement", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<MovementType>> findAllMovementTypes() {
		List<MovementType> movementTypesList=movementTypeServiceImpl.findAllMovementTypes();
		return new ResponseEntity<List<MovementType>>(movementTypesList,HttpStatus.OK);
	}
}
