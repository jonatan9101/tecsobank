package coop.tecso.examen.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.model.AccountOwner;
import coop.tecso.examen.service.impl.AccountOwnerServiceImpl;
import coop.tecso.examen.shared.enums.TypePerson;
import coop.tecso.examen.shared.exceptions.InvalidRequestFieldException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/accountowner")
public class AccountOwnerController {

	@Autowired
	private AccountOwnerServiceImpl accountOwnerServiceImpl;
	
	
	@PostMapping(value="")
	public ResponseEntity<Long> saveAccountOwner(@Valid @RequestBody AccountOwner accountOwner) {
		validateAccountOwner(accountOwner);
		if(accountOwnerServiceImpl.findByRut(accountOwner.getRut())!=null) 
			throw new InvalidRequestFieldException("El RUT enviado ya se encuentra registrado",HttpStatus.CONFLICT);
		accountOwnerServiceImpl.save(accountOwner);
		return new ResponseEntity<Long>(accountOwner.getId(),HttpStatus.CREATED);
	}
	
	@PutMapping(value="")
	public ResponseEntity<Long> editAccountOwner(@Valid @RequestBody AccountOwner accountOwner) {
		validateAccountOwner(accountOwner);
		AccountOwner currentAccountOwner=accountOwnerServiceImpl.findById(accountOwner.getId());
		if(currentAccountOwner==null)
			throw new InvalidRequestFieldException("El id "+accountOwner.getId()+" no se encuentra registrado como titular",HttpStatus.NOT_FOUND);
		
		currentAccountOwner.setLegalPerson(accountOwner.getLegalPerson());
		currentAccountOwner.setNaturalPerson(accountOwner.getNaturalPerson());
		currentAccountOwner.setTypePerson(accountOwner.getTypePerson());
		accountOwnerServiceImpl.save(currentAccountOwner);
		return new ResponseEntity<Long>(currentAccountOwner.getId(),HttpStatus.FOUND);
	}	
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Long> deleteAccountOwner(@PathVariable("id") long id) {
		AccountOwner currentaccountOwner=accountOwnerServiceImpl.findById(id);
		if(currentaccountOwner==null)
			throw new InvalidRequestFieldException("El id "+id+" no se encuentra registrado como titular",HttpStatus.NOT_FOUND);
		
		accountOwnerServiceImpl.deleteById(id);
		return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(value = "",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AccountOwner>> getAllAccountOwner() {
		List<AccountOwner> accountOwnersList=accountOwnerServiceImpl.findAllAccountOwners();
		return new ResponseEntity<List<AccountOwner>>(accountOwnersList,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AccountOwner> getByIdAccountOwner(@PathVariable("id") long id) {
		AccountOwner accountOwner=accountOwnerServiceImpl.findById(id);
		return new ResponseEntity<AccountOwner>(accountOwner,HttpStatus.OK);
	}
	
	
	private void validateAccountOwner(AccountOwner accountOwner) {
		if((accountOwner.getNaturalPerson()==null && accountOwner.getLegalPerson()==null))
			throw new InvalidRequestFieldException("No ha enviado los datos requeridos por el titular",HttpStatus.BAD_REQUEST);
		if((accountOwner.getTypePerson().getId()==TypePerson.NATURAL.getId() && accountOwner.getNaturalPerson()==null) ||
				(accountOwner.getTypePerson().getId()==TypePerson.LEGAL.getId() && accountOwner.getLegalPerson()==null))
			throw new InvalidRequestFieldException("No ha enviado los datos requeridos por el titular, para el tipo de persona con id "+accountOwner.getTypePerson().getId(),HttpStatus.BAD_REQUEST);
		if((accountOwner.getNaturalPerson()!=null && accountOwner.getLegalPerson()!=null))
			throw new InvalidRequestFieldException("Ha enviado datos para persona natural y persona legal en el request",HttpStatus.BAD_REQUEST);
	}
	
	
	
}
