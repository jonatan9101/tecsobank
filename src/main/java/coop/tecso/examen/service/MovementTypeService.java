package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.MovementType;


public interface MovementTypeService {

	MovementType findById(long id);
	
	List<MovementType> findAllMovementTypes();
	
}
