package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.Account;
import coop.tecso.examen.model.Movement;


public interface MovementService {

	List<Movement> findByAccountOrderByMovementDateDesc(Account account);
	
	void save(Movement movement);

}
