package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.PersonType;

public interface TypePersonService {

	PersonType findById(long id);
	
	List<PersonType> findAllTypePersons();
	
	
	
	
	
}
