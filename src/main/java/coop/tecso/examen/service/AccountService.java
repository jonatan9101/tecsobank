package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.Account;


public interface AccountService {

	Account findById(long id);
	
	List<Account> findAllAccounts();
	
	void save(Account account);
	
	void deleteById(long id);
	
	Account findByAccountNumber(long accountNumber);
	
}
