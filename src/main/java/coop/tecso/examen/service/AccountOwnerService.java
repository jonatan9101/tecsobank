package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.AccountOwner;


public interface AccountOwnerService {

	AccountOwner findById(long id);
	
	List<AccountOwner> findAllAccountOwners();
	
	void save(AccountOwner accountOwner);
	
	AccountOwner findByRut(String rut);
	
	void deleteById(long id);
	
	
	
	
	
}
