package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.model.MovementType;
import coop.tecso.examen.repository.MovementTypeRepository;
import coop.tecso.examen.service.MovementTypeService;

@Service
public class MovementTypeServiceImpl implements MovementTypeService {

	@Autowired
	private MovementTypeRepository MovementTypeRepository;


	@Override
	public MovementType findById(long id) {
		return MovementTypeRepository.findById(id).get();
	}
	
	public List<MovementType> findAllMovementTypes(){
		return (List<MovementType>) MovementTypeRepository.findAll();
	}
	

}
