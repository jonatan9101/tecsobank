package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import coop.tecso.examen.model.Account;
import coop.tecso.examen.repository.AccountRepository;
import coop.tecso.examen.service.AccountService;


@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	private AccountRepository  accountRepository;

	@Override
	public Account findById(long id) {
		return accountRepository.findById(id).orElse(null);
	}

	@Override
	public List<Account> findAllAccounts() {
		return (List<Account>)accountRepository.findAll();
	}

	@Override
	public void save(Account account) {
		accountRepository.save(account);
		
	}

	@Override
	public void deleteById(long id) {
		accountRepository.deleteById(id);
	}

	@Override
	public Account findByAccountNumber(long accountNumber) {
		return accountRepository.findByAccountNumber(accountNumber);
	}

}
