package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import coop.tecso.examen.model.AccountOwner;
import coop.tecso.examen.repository.AccountOwnerRepository;
import coop.tecso.examen.service.AccountOwnerService;


@Service
public class AccountOwnerServiceImpl implements AccountOwnerService{

	@Autowired
	private AccountOwnerRepository  accountOwnerRepository;

	@Override
	public AccountOwner findById(long id) {
		return accountOwnerRepository.findById(id).orElse(null);
	}

	@Override
	public List<AccountOwner> findAllAccountOwners() {
		return (List<AccountOwner>)accountOwnerRepository.findAll();
	}

	@Override
	public void save(AccountOwner accountOwner) {
		accountOwnerRepository.save(accountOwner);
		
	}

	@Override
	public AccountOwner findByRut(String rut) {
		return accountOwnerRepository.findByRut(rut);
	}

	@Override
	public void deleteById(long id) {
		accountOwnerRepository.deleteById(id);
	}



	

}
