package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.model.Currency;
import coop.tecso.examen.repository.CurrencyRepository;
import coop.tecso.examen.service.CurrencyService;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyRepository CurrencyRepository;


	@Override
	public Currency findById(long id) {
		return CurrencyRepository.findById(id).get();
	}
	
	public List<Currency> findAllCurrencies(){
		return (List<Currency>) CurrencyRepository.findAll();
	}
	

}
