package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import coop.tecso.examen.model.PersonType;
import coop.tecso.examen.repository.TypePersonRepository;
import coop.tecso.examen.service.TypePersonService;

@Service
public class TypePersonServiceImpl implements TypePersonService {

	@Autowired
	private TypePersonRepository typePersonRepository;


	@Override
	public PersonType findById(long id) {
		return typePersonRepository.findById(id).get();
	}
	
	public List<PersonType> findAllTypePersons(){
		return (List<PersonType>) typePersonRepository.findAll();
	}
	

}
