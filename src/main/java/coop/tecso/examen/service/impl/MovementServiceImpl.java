package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.model.Account;
import coop.tecso.examen.model.Movement;
import coop.tecso.examen.repository.MovementRepository;
import coop.tecso.examen.service.MovementService;


@Service
public class MovementServiceImpl implements MovementService{

	@Autowired
	private MovementRepository  movementRepository;

	@Override
	public List<Movement> findByAccountOrderByMovementDateDesc(Account account) {
		return movementRepository.findByAccountOrderByMovementDateDesc(account);
	}

	@Override
	public void save(Movement movement) {
		movementRepository.save(movement);
		
	}


	

}
