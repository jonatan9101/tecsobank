package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.Currency;


public interface CurrencyService {

	Currency findById(long id);
	
	List<Currency> findAllCurrencies();
	
}
