/**
 * 
 */
package coop.tecso.examen.service;

/**
 * @author Jonatan Espinosa
 *
 */
import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if ("administrator".equals(username)) {
			return new User("administrator", "$2a$10$k2dxOoeA7FbTbeRm3tzcweSBPqepzxOta.Bnu4EepEypWAKFXjDbe",
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("Usuario con nombre" + username+" no se encuentra registrado");
		}
	}
}
