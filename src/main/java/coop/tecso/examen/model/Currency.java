package coop.tecso.examen.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Currency extends AbstractPersistentObject {
	
	private static final long serialVersionUID = -8901155893511467206L;
	
	private String currency;
	
	private String shortName;
	
	private double minBalance;
	
	
	@JsonIgnore
    @OneToMany(mappedBy = "currency")
	private List<Account> accountsList;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public double getMinBalance() {
		return minBalance;
	}

	public void setMinBalance(double minBalance) {
		this.minBalance = minBalance;
	}
	
	

}
