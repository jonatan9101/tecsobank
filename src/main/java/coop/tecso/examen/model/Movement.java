package coop.tecso.examen.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Movement extends AbstractPersistentObject {

	private static final long serialVersionUID = -8901155893511467206L;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date movementDate;
	
	@ManyToOne
	private MovementType movementType;
	
	@Length(min = 1, max = 200)
	private String description;
	
	@Column(precision=10, scale=2)
	private double amount;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToOne
	private Account account;

	public Date getMovementDate() {
		return movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public MovementType getMovementType() {
		return movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	
	
}
