package coop.tecso.examen.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
public class AccountOwner extends AbstractPersistentObject{

	private static final long serialVersionUID = -8901155893511467206L;
	
	@Column(nullable= false, unique = true)
	@NotBlank(message = "Rut es requerido")
	private String rut;
	
	@ManyToOne    
	private PersonType typePerson;
	
	@OneToOne (optional=true)
	@Cascade({CascadeType.ALL})
	private NaturalPerson naturalPerson;
	
	@OneToOne(optional=true)
	@Cascade({CascadeType.ALL})
	private LegalPerson legalPerson;

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public PersonType getTypePerson() {
		return typePerson;
	}

	public void setTypePerson(PersonType typePerson) {
		this.typePerson = typePerson;
	}

	public NaturalPerson getNaturalPerson() {
		return naturalPerson;
	}

	public void setNaturalPerson(NaturalPerson naturalPerson) {
		this.naturalPerson = naturalPerson;
	}

	public LegalPerson getLegalPerson() {
		return legalPerson;
	}

	public void setLegalPerson(LegalPerson legalPerson) {
		this.legalPerson = legalPerson;
	}
	
	
	
	
}
