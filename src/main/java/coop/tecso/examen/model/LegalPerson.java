package coop.tecso.examen.model;

import java.sql.Date;

import javax.persistence.Entity;


import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class LegalPerson extends AbstractPersistentObject {
	
	private static final long serialVersionUID = -8901155893511467206L;

	@Length(min = 1, max = 100)
	private String businessName;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone="EST")
	private Date foundationDate;
	

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public Date getFoundationDate() {
		return foundationDate;
	}

	public void setFoundationDate(Date foundationDate) {
		this.foundationDate = foundationDate;
	}


}
