package coop.tecso.examen.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;



@Entity
public class Account extends AbstractPersistentObject{

	private static final long serialVersionUID = -8901155893511467206L;
	
	@Column(nullable = false, unique = true)
	
	private long accountNumber;
	
	@ManyToOne(optional = false)
	private Currency currency;
	
	@Column(nullable = false,scale=2)
	private double balance;
	
	@OneToMany(mappedBy = "account")
	private List<Movement> movements;

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public List<Movement> getMovements() {
		return movements;
	}

	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}
	
	
	
}
