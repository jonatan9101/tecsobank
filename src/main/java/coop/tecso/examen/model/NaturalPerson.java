package coop.tecso.examen.model;

import javax.persistence.Entity;

import org.hibernate.validator.constraints.Length;

@Entity
public class NaturalPerson extends AbstractPersistentObject {
	
	private static final long serialVersionUID = -8901155893511467206L;

	@Length(min = 1, max = 80)
	private String name;
	
	@Length(min = 1, max = 250)
	private String lastName;
	
	private long dni;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getDni() {
		return dni;
	}

	public void setDni(long dni) {
		this.dni = dni;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
