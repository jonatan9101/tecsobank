package coop.tecso.examen.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PersonType extends AbstractPersistentObject {
	
	private static final long serialVersionUID = -8901155893511467206L;
	
	private String descriptionType;
	
	
	@JsonIgnore
    @OneToMany(mappedBy = "typePerson")
	private List<AccountOwner> accountOwnersList;

	public String getDescriptionType() {
		return descriptionType;
	}

	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}
	
	

}
